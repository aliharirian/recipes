const list = document.querySelector('ul');
const form = document.querySelector('form');

const addRecipe = (recipe, id)=>{

    let html = `
    <li data-id="${id}">
    <div>${recipe.Title}</div>
    <div>${recipe.Created_at.toDate()}</div>
    <button class="btn btn-danger btn-sm my-2">delete</button>
    </li>
    `;
    list.innerHTML += html  ;

}

const deleteRecipeFromDOM = (id) => {
    const recipes = document.querySelectorAll('li');
    recipes.forEach(recipe => {
        if(recipe.getAttribute('data-id') === id){
            recipe.remove();
        }
    });
}
// Get documents (old code)
// db.collection('Recipes').get().then((snapshot)=>{
//     //when we have the data
//     snapshot.docs.forEach(element => {
        
//         addRecipe(element.data(), element.id);
        
//     });

// }).catch((err)=>{
//     alarm(err);
// });

//Real time listerener

db.collection('Recipes').onSnapshot(snapshot =>{
    snapshot.docChanges().forEach( change => {
        const doc = change.doc;
        if( change.type === 'added'){
            addRecipe(doc.data(), doc.id);
        } else if (change.type === 'removed'){
            deleteRecipeFromDOM(doc.id);
        }
    });
});

form.addEventListener('submit',(e)=>{
    e.preventDefault();
    const now = new Date();
    const recipe = {
        Title: form.recipe.value,
        Created_at: firebase.firestore.Timestamp.fromDate(now)
    };

    db.collection('Recipes').add(recipe).then(()=>{

       
    }).catch(err =>{
        alert(err);
    });

});

// delete data
list.addEventListener('click', e =>{
    if(e.target.tagName==='BUTTON'){
        const id = e.target.parentElement.getAttribute('data-id');
        db.collection('Recipes').doc(id).delete().then(()=>{
          
        });
    }
});